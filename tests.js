function launchTests(removedContent) {
	removedContent.parentNode.removeChild(removedContent);
	mocha.run();
}

function prepareTests() {
	var expect = chai.expect;

	// describe('Array', function() {
	// 	it('Should be empty', function() {
	// 		var arr = [];
	// 		expect(arr.length).to.equal(0);
	// 	});
	// });
}

document.addEventListener('DOMContentLoaded', function() {

	var testButton = document.getElementById('start-test');
	testButton.addEventListener('click', launchTests.bind(null, testButton));
	
	mocha.setup('bdd');
	prepareTests();

});


